package co.com.ceiba.mobile.pruebadeingreso.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import co.com.ceiba.mobile.pruebadeingreso.R;
import co.com.ceiba.mobile.pruebadeingreso.model.PostClass;

public class AdapterPublications extends RecyclerView.Adapter<AdapterPublications.Viewholder> {

    private List<PostClass> postClassList;

    public AdapterPublications(List<PostClass> postClassList) {
        this.postClassList = postClassList;
    }

    @NonNull
    @Override
    public Viewholder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.post_list_item, viewGroup, false);
        return new Viewholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Viewholder viewholder, int position) {
        int id = postClassList.get(position).getId();
        String title = postClassList.get(position).getTitle();
        String body = postClassList.get(position).getBody();
        viewholder.setData(title, body);

    }

    @Override
    public int getItemCount() {
        return postClassList.size();
    }


    class Viewholder extends RecyclerView.ViewHolder{

        private TextView title;
        private TextView body;

        public Viewholder(View itemView) {
            super(itemView);

            title = itemView.findViewById(R.id.title);
            body = itemView.findViewById(R.id.body);
        }

        private void setData(String titleText, String bodyText){
            title.setText(titleText);
            body.setText(bodyText);
        }
    }
}
