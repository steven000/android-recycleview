package co.com.ceiba.mobile.pruebadeingreso.adapters;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import co.com.ceiba.mobile.pruebadeingreso.R;
import co.com.ceiba.mobile.pruebadeingreso.model.ItemClass;
import co.com.ceiba.mobile.pruebadeingreso.view.PostActivity;

public class Adapter extends RecyclerView.Adapter<Adapter.Viewholder> implements Filterable {

    private List<ItemClass> itemClassList;
    private List<ItemClass> itemClassListFilter;

    public Adapter(List<ItemClass> itemClassList) {
        this.itemClassList = itemClassList;
        itemClassListFilter = new ArrayList<>(itemClassList);
    }

    @NonNull
    @Override
    public Viewholder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.user_list_item, viewGroup, false);
        return new Viewholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Viewholder viewholder, int position) {

        final int p = position;
        String name = itemClassList.get(position).getName();
        String phone = itemClassList.get(position).getPhone();
        String email = itemClassList.get(position).getEmail();
        viewholder.setData(name, phone, email);

        viewholder.publications.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), PostActivity.class);
                intent.putExtra("itemName", itemClassList.get(p).getName());
                intent.putExtra("itemPhone", itemClassList.get(p).getPhone());
                intent.putExtra("itemEmail", itemClassList.get(p).getEmail());
                intent.putExtra("itemUserId", itemClassList.get(p).getId());
                v.getContext().startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return itemClassList.size();
    }

    class Viewholder extends RecyclerView.ViewHolder{

        private TextView name;
        private TextView phone;
        private TextView email;
        private Button publications;

        public Viewholder(View itemView) {
            super(itemView);

            name = itemView.findViewById(R.id.name);
            phone = itemView.findViewById(R.id.phone);
            email = itemView.findViewById(R.id.email);
            publications = itemView.findViewById(R.id.btn_view_post);
        }

        private void setData(String nameText, String phoneText, String emailText){
            name.setText(nameText);
            phone.setText(phoneText);
            email.setText(emailText);
        }
    }

    @Override
    public Filter getFilter() {
        return filterUsers;
    }

    private Filter filterUsers = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<ItemClass> filteredList = new ArrayList<>();

            if(constraint == null || constraint.length() == 0){
                filteredList.addAll(itemClassListFilter );
            }else{
                String filterPattern = constraint.toString().toLowerCase().trim();

                for ( ItemClass item: itemClassListFilter){
                    if(item.getName().toLowerCase().contains(filterPattern)){
                        filteredList.add(item);

                    }
                }
            }
            FilterResults results = new FilterResults();
            results.values = filteredList;

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            itemClassList.clear();
            itemClassList.addAll((List) results.values);
            notifyDataSetChanged();
        }
    };
}
